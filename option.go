package worker

import (
	"context"
	"sync"
)

type OptionFunc func(w *Worker)

func WithContext(ctx context.Context, cancelFunc context.CancelFunc) OptionFunc {
	return func(w *Worker) {
		w.ctx = ctx
		w.cancelFunc = cancelFunc
	}
}

func WithStartWaiter(startWaiter *sync.WaitGroup) OptionFunc {
	return func(w *Worker) {
		w.startWg = startWaiter
	}
}

func WithStopWaiter(stopWaiter *sync.WaitGroup) OptionFunc {
	return func(w *Worker) {
		w.stopWg = stopWaiter
	}
}

func WithOnStarted(onStarted func()) OptionFunc {
	return func(w *Worker) {
		w.onStartedFunc = onStarted
	}
}

func WithOnWorking(onWorking func(ctx context.Context) bool) OptionFunc {
	return func(w *Worker) {
		w.onWorkingFunc = onWorking
	}
}

func WithOnStop(onStopping func()) OptionFunc {
	return func(w *Worker) {
		w.onStopFunc = onStopping
	}
}

func WithOnError(onError func(p any) (err error)) OptionFunc {
	return func(w *Worker) {
		w.onErrorFunc = onError
	}
}
